from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views


urlpatterns = [
    url(r'^user/$', views.UserList.as_view()),
    url(r'^user/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
    url(r'^useravatar/$', views.UserAvatarCreate.as_view()),
    url(r'^facebook/login/$', views.FacebookLogin.as_view(), name='fb_login'),
    url(r'^weibo/login/$', views.WeiboLogin.as_view(), name='wb_login')
]

urlpatterns = format_suffix_patterns(urlpatterns)
