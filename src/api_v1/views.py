from allauth.socialaccount.providers.weibo.views import WeiboOAuth2Adapter
from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth import get_user_model
from django.utils import timezone

from rest_auth.app_settings import TokenSerializer
from rest_framework import generics
from rest_framework import permissions
from rest_framework import status
from rest_framework import filters
from rest_framework.exceptions import ValidationError
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from rest_auth.registration.views import SocialLoginView

from src.api_v1.filters import UserFilter
from src.api_v1.status import ApiStatus, PASSWORD_CHANGE_SUCCESS

from .permissions import IsSuperUser
from .serializers import UserSerializer, UserAvatarSerializer, UserDetailSerializer, LoginSerializer, \
    PasswordChangeSerializer

USER_MODEL = get_user_model()


class LoginView(generics.GenericAPIView):

    """
    Check the credentials and return the REST Token
    if the credentials are valid and authenticated.
    Calls Django Auth login method to register User ID
    in Django session framework

    Accept the following POST parameters: username, password
    Return the REST Framework Token Object's key.
    """
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer
    token_model = Token
    response_serializer = TokenSerializer

    def login(self):
        self.user = self.serializer.validated_data['user']
        self.token, created = self.token_model.objects.get_or_create(
            user=self.user)
        if getattr(settings, 'REST_SESSION_LOGIN', True):
            login(self.request, self.user)
        else:
            self.user.last_login = timezone.now()
            self.user.save(update_fields=['last_login'])

    def get_response(self):
        result = self.user.dict()
        result['key'] = self.token.key
        return Response(result, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        self.serializer = self.get_serializer(data=self.request.data)
        try:
            self.serializer.is_valid(raise_exception=True)
        except Exception as e:
            if isinstance(e, ValidationError):
                return Response(e.detail, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                return Response(e.message, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        self.login()
        return self.get_response()


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000


class UserList(generics.ListAPIView):
    queryset = USER_MODEL.objects.select_related().all()
    serializer_class = UserSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('is_active', 'is_superuser', 'deleted', 'phone', 'email')
    filter_class = UserFilter
    search_fields = ('username', 'first_name', 'last_name', 'email', 'phone')
    ordering_fields = ('username', 'first_name', 'last_name', 'email',
                       'phone', 'date_joined', 'last_login', 'is_active')


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = USER_MODEL.objects.select_related().all()
    serializer_class = UserDetailSerializer
    permission_classes = (permissions.IsAuthenticated, IsSuperUser)

    def perform_destroy(self, instance):
        instance.deleted = True
        instance.save()


class UserAvatarCreate(generics.CreateAPIView):
    serializer_class = UserAvatarSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        picture_file = self.request.FILES.get('picture')

        size = picture_file.size
        width_height = picture_file.image.size
        width = width_height[0]
        height = width_height[1]
        p = serializer.save(user=self.request.user, size=size, width=width, height=height)
        p.user.avatar_url = p.get_small_thumbnail(self.request)
        p.user.save()


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class WeiboLogin(SocialLoginView):
    adapter_class = WeiboOAuth2Adapter


class PasswordChangeView(generics.GenericAPIView):

    """
    Calls Django Auth SetPasswordForm save method.

    Accepts the following POST parameters: new_password1, new_password2
    Returns the success/fail message.
    """

    serializer_class = PasswordChangeSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except Exception as e:
            if isinstance(e, ValidationError):
                return Response(e.detail, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                return Response(e.message, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        serializer.save()
        return Response(ApiStatus(PASSWORD_CHANGE_SUCCESS).get_json())
