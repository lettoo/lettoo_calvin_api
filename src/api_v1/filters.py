import django_filters
from django.contrib.auth import get_user_model
from django.db.models import Q

USER_MODEL = get_user_model()


class UserFilter(django_filters.FilterSet):
    # phone = django_filters.CharFilter(name='phone', lookup_expr='icontains')
    # no_phone = django_filters.BooleanFilter(name='phone', lookup_expr='isnull')
    # no_email = django_filters.BooleanFilter(name='email__isnull')
    # is_active = django_filters.BooleanFilter(name='is_active')
    # is_superuser = django_filters.BooleanFilter(name='is_superuser')
    # deleted = django_filters.BooleanFilter(name='deleted')

    phone__isnull = django_filters.MethodFilter(action='no_phone_filter')
    email__isnull = django_filters.MethodFilter(action='no_email_filter')

    class Meta:
        model = USER_MODEL

    def no_phone_filter(self, queryset, value):
        if value == 'True':
            return queryset.filter(Q(phone__isnull=True) | Q(phone=''))
        else:
            return queryset.filter(phone__isnull=False).exclude(phone='')

    def no_email_filter(self, queryset, value):
        if value == 'True':
            return queryset.filter(Q(email__isnull=True) | Q(email=''))
        else:
            return queryset.filter(email__isnull=False).exclude(email='')
