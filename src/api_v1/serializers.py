# -*- coding: utf-8 -*-
from collections import OrderedDict

from django.conf import settings
from allauth.account.forms import ResetPasswordForm
from django.contrib.auth import get_user_model
from django.utils.module_loading import import_string
from rest_auth.serializers import PasswordChangeSerializer
from rest_framework import serializers
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.exceptions import ValidationError
from rest_framework.fields import set_value
from rest_framework.settings import api_settings
from src.api_v1.status import ApiStatus, USER_ACCOUNT_DISABLED, USER_ACCOUNT_DELETED, USERNAME_OR_PASSWORD_INCORRECT, \
    USERNAME_AND_PASSWORD_REQUIRED, EMAIL_OR_PHONE_IS_NOT_VERIFIED, EMAIL_AND_PHONE_IS_NOT_VERIFIED, \
    EMAIL_IS_NOT_VERIFIED, PHONE_IS_NOT_VERIFIED, PASSWORD_INVALID, PASSWORD_CHANGE_PARAMS_INCORRECT

from src.users.models import check_avatar_exist_with_md5, UserAvatar
from src.api_v1.utils import get_file_md5
from src.api_v1.forms import SetPasswordForm

USER_MODEL = get_user_model()


class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""

    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        return value


class LoginSerializer(AuthTokenSerializer):
    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user, ret_type = self._authenticate(username=username, password=password)

            if user:
                if not user.is_active:
                    raise Exception(ApiStatus(USER_ACCOUNT_DISABLED).get_json())
                elif user.deleted:
                    raise Exception(ApiStatus(USER_ACCOUNT_DELETED).get_json())
                else:
                    self._authentication_method(user, ret_type)
                    attrs['user'] = user
            else:
                raise Exception(ApiStatus(USERNAME_OR_PASSWORD_INCORRECT).get_json())
        else:
            raise Exception(ApiStatus(USERNAME_AND_PASSWORD_REQUIRED).get_json())

        return attrs

    def to_internal_value(self, data):
        """
        Dict of native values <- Dict of primitive datatypes.
        """
        if not isinstance(data, dict):
            message = self.error_messages['invalid'].format(
                datatype=type(data).__name__
            )
            raise ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [message]
            })

        ret = OrderedDict()
        errors = OrderedDict()
        fields = self._writable_fields

        for field in fields:
            validate_method = getattr(self, 'validate_' + field.field_name, None)
            primitive_value = field.get_value(data)
            try:
                validated_value = field.run_validation(primitive_value)
                if validate_method is not None:
                    validated_value = validate_method(validated_value)
            except Exception as exc:
                errors = ApiStatus(USERNAME_OR_PASSWORD_INCORRECT).get_json()
            else:
                set_value(ret, field.source_attrs, validated_value)

        if errors:
            raise Exception(errors)

        return ret

    def _authenticate(self, **credentials):
        """
        If the given credentials are valid, return a User object.
        """
        backend_path = 'src.api_v1.backends.MutipleAuthenticationBackend'
        backend = import_string(backend_path)()

        user, ret_type = backend.authenticate(**credentials)
        if user:
            user.backend = backend_path
        return user, ret_type

    def _authentication_method(self, user, ret_type):
        OR = '|'
        AND = '&'

        ret_verified = True

        method = getattr(settings, ('%s_LOGIN_AUTHENTICATION_METHOD' % ret_type.upper()))

        if method.find(OR) != -1:
            method_array = method.split(OR)
            separator = OR
        elif method.find(AND) != -1:
            method_array = method.split(AND)
            separator = AND
        else:
            method_array = [method]
            separator = None

        for item in method_array:
            if item.lower() == 'email':
                verified = user.emailaddress_set.filter(email=user.email, verified=True).exists()
                if not verified:
                    verified = user.quickemailaddress_set.filter(email=user.email, verified=True).exists()
            elif item.lower() == 'phone':
                verified = user.quickphonenumber_set.filter(phone=user.phone, verified=True).exists()

            if verified:
                ret_verified = True
                if separator == AND:
                    continue
                else:
                    break
            else:
                ret_verified = False
                if separator == AND:
                    break
                else:
                    continue

        if not ret_verified:
            if separator == OR:
                raise Exception(ApiStatus(EMAIL_OR_PHONE_IS_NOT_VERIFIED).get_json())
            elif separator == AND:
                raise Exception(ApiStatus(EMAIL_AND_PHONE_IS_NOT_VERIFIED).get_json())
            else:
                if method_array[0].lower() == 'email':
                    raise Exception(ApiStatus(EMAIL_IS_NOT_VERIFIED).get_json())
                elif method_array[0].lower() == 'phone':
                    raise Exception(ApiStatus(PHONE_IS_NOT_VERIFIED).get_json())

        return ret_verified


class PasswordResetSerializer(serializers.Serializer):
    """
    Serializer for requesting a password reset e-mail.
    """

    email = serializers.EmailField()

    password_reset_form_class = ResetPasswordForm

    def validate_email(self, value):
        # Create PasswordResetForm with the serializer
        self.reset_form = self.password_reset_form_class(data=self.initial_data)
        if not self.reset_form.is_valid():
            raise serializers.ValidationError('Error')
        return value

    def save(self):
        request = self.context.get('request')
        # Set some values to trigger the send_email method.
        opts = {
            'use_https': request.is_secure(),
            'from_email': getattr(settings, 'DEFAULT_FROM_EMAIL'),
            'request': request,
        }
        self.reset_form.save(**opts)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = USER_MODEL
        exclude = ('password', 'groups', 'user_permissions')


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = USER_MODEL
        exclude = ('password', 'groups', 'user_permissions')


class AuthUserDetailSerializer(serializers.ModelSerializer):
    username = serializers.ReadOnlyField()
    is_superuser = serializers.ReadOnlyField()
    is_staff = serializers.ReadOnlyField()
    is_active = serializers.ReadOnlyField()
    deleted = serializers.ReadOnlyField()
    last_login = serializers.ReadOnlyField()
    date_joined = serializers.ReadOnlyField()
    phone_verified = serializers.ReadOnlyField()
    email_verified = serializers.ReadOnlyField()
    has_usable_password = serializers.SerializerMethodField()

    class Meta:
        model = USER_MODEL
        exclude = ('password', 'groups', 'user_permissions')

    def get_has_usable_password(self, obj):
        return obj.has_usable_password()


class UserAvatarSerializer(serializers.ModelSerializer):
    width = serializers.ReadOnlyField()
    height = serializers.ReadOnlyField()
    size = serializers.ReadOnlyField()
    avatar_url = serializers.SerializerMethodField()
    md5 = serializers.ReadOnlyField()

    def create(self, validated_data):
        picture_file = validated_data['picture']
        md5 = get_file_md5(picture_file)
        p, exist = check_avatar_exist_with_md5(validated_data['user'], md5)
        if exist:
            return p

        validated_data['md5'] = md5
        p = super(UserAvatarSerializer, self).create(validated_data)
        return p

    def get_avatar_url(self, obj):
        request = self.context.get('request')
        return obj.get_small_thumbnail(request)

    class Meta:
        model = UserAvatar


class PasswordChangeSerializer(serializers.Serializer):

    old_password = serializers.CharField(max_length=128)
    new_password1 = serializers.CharField(max_length=128)
    new_password2 = serializers.CharField(max_length=128)

    set_password_form_class = SetPasswordForm

    def __init__(self, *args, **kwargs):
        super(PasswordChangeSerializer, self).__init__(*args, **kwargs)

        self.request = self.context.get('request')
        self.user = getattr(self.request, 'user', None)
        self.old_password_field_enabled = self.user.has_usable_password()
        self.logout_on_password_change = False

        if not self.old_password_field_enabled:
            self.fields.pop('old_password')

    def validate_old_password(self, value):
        invalid_password_conditions = (
            self.old_password_field_enabled,
            self.user,
            not self.user.check_password(value)
        )

        if all(invalid_password_conditions):
            raise Exception(ApiStatus(PASSWORD_INVALID).get_json())

        return value

    def validate(self, attrs):
        self.set_password_form = self.set_password_form_class(
            user=self.user, data=attrs
        )

        if not self.set_password_form.is_valid():
            raise Exception(self.set_password_form.errors)
        return attrs

    def save(self):
        self.set_password_form.save()
        if not self.logout_on_password_change:
            from django.contrib.auth import update_session_auth_hash
            update_session_auth_hash(self.request, self.user)

    def to_internal_value(self, data):
        """
        Dict of native values <- Dict of primitive datatypes.
        """
        if not isinstance(data, dict):
            message = self.error_messages['invalid'].format(
                datatype=type(data).__name__
            )
            raise ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [message]
            })

        ret = OrderedDict()
        errors = OrderedDict()
        fields = self._writable_fields

        for field in fields:
            validate_method = getattr(self, 'validate_' + field.field_name, None)
            primitive_value = field.get_value(data)
            try:
                validated_value = field.run_validation(primitive_value)
                if validate_method is not None:
                    validated_value = validate_method(validated_value)
            except Exception as exc:
                errors = ApiStatus(PASSWORD_CHANGE_PARAMS_INCORRECT).get_json()
            else:
                set_value(ret, field.source_attrs, validated_value)

        if errors:
            raise Exception(errors)

        return ret
