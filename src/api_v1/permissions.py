from django.contrib.auth import get_user_model
from rest_framework import permissions

USER_MODEL = get_user_model()


class IsOwnerOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to access it.
    """

    def has_object_permission(self, request, view, obj):
        # All permissions are only allowed to the owner of the snippet.
        request_user = request.user

        if request_user.is_staff or request_user.is_superuser:
            return True
        else:
            if isinstance(obj, USER_MODEL) and obj == request.user:
                return True
            else:
                return obj.user == request.user


class IsSuperUser(permissions.BasePermission):
    """
    Allows access only to admin users.
    """

    def has_permission(self, request, view):
        return request.user and (request.user.is_staff or request.user.is_superuser)
