# -*- coding: utf-8 -*-
import hashlib
import base64
from allauth.utils import build_absolute_uri
from django.core.urlresolvers import reverse
from Crypto import Random
from Crypto.Cipher import AES


def get_file_md5(file):
    myhash = hashlib.md5()
    while True:
        b = file.read(8096)
        if not b:
            break
        myhash.update(b)
    # file.close()
    file.seek(0, 0)
    return myhash.hexdigest()


def _pad(s):
    bs = 16
    return s + (bs - len(s) % bs) * chr(bs - len(s) % bs)


def _unpad(s):
    return s[:-ord(s[len(s) - 1:])]


def encrypt_aes(key, text):
    try:
        raw = _pad(text)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))
    except Exception as e:
        print e.message
        return text


def decrypt_aes(key, text):
    try:
        enc = base64.b64decode(text)
        iv = enc[:AES.block_size]
        cipher = AES.new(key, AES.MODE_CBC, iv)
        return _unpad(cipher.decrypt(enc[AES.block_size:]))
    except Exception as e:
        print e.message
        return text
