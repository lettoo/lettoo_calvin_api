from __future__ import absolute_import

from django import forms
from django.forms import FileField

from rest_framework.exceptions import ValidationError

from src.api_v1.status import ApiStatus, PASSWORD_FIELD_REQUIRED, PASSWORD_CONFIRMATION_FIELD_REQUIRED, \
    PASSWORD_MISMATCH


class SetPasswordForm(forms.Form):
    new_password1 = forms.CharField(label='New password', required=False, max_length=256)
    new_password2 = forms.CharField(label='New password confirmation', required=False, max_length=256)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SetPasswordForm, self).__init__(*args, **kwargs)

    def clean_new_password1(self):
        password1 = self.cleaned_data.get('new_password1')

        if not password1:
            raise Exception(ApiStatus(PASSWORD_FIELD_REQUIRED).get_json())

        return password1

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')

        if not password1:
            raise Exception(ApiStatus(PASSWORD_FIELD_REQUIRED).get_json())

        if not password2:
            raise Exception(ApiStatus(PASSWORD_CONFIRMATION_FIELD_REQUIRED).get_json())

        if password1 != password2:
            raise Exception(ApiStatus(PASSWORD_MISMATCH).get_json())

        return password2

    def _clean_fields(self):
        for name, field in self.fields.items():
            # value_from_datadict() gets the data from the data dictionaries.
            # Each widget type knows how to retrieve its own data, because some
            # widgets split data over several HTML fields.
            if field.disabled:
                value = self.initial.get(name, field.initial)
            else:
                value = field.widget.value_from_datadict(self.data, self.files, self.add_prefix(name))
            try:
                if isinstance(field, FileField):
                    initial = self.initial.get(name, field.initial)
                    value = field.clean(value, initial)
                else:
                    value = field.clean(value)
                self.cleaned_data[name] = value
                if hasattr(self, 'clean_%s' % name):
                    value = getattr(self, 'clean_%s' % name)()
                    self.cleaned_data[name] = value
            except ValidationError as e:
                self.add_error(name, e)
            except Exception as e:
                self._errors = e.message

    def save(self, commit=True):
        password = self.cleaned_data["new_password2"]
        self.user.set_password(password)
        if commit:
            self.user.save()
        return self.user
