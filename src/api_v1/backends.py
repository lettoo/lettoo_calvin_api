from allauth.utils import get_user_model
from allauth.account.utils import filter_users_by_email
from allauth.account import app_settings
from django.contrib.auth.backends import ModelBackend

from phone_quick_signup import app_settings as phone_quick_signup_app_settings


class MutipleAuthenticationBackend(ModelBackend):
    def authenticate(self, **credentials):
        # Email authenticate
        ret, ret_type = self._authenticate_by_email(**credentials)

        # Phone authenticate
        if not ret:
            ret, ret_type = self._authenticate_by_phone(**credentials)

        # Username authenticate
        if not ret:
            ret, ret_type = self._authenticate_by_username(**credentials)


        return ret, ret_type

    def _authenticate_by_username(self, **credentials):
        username_field = app_settings.USER_MODEL_USERNAME_FIELD
        username = credentials.get('username')
        password = credentials.get('password')
        ret_type = 'username'

        User = get_user_model()

        if not username_field or username is None or password is None:
            return None, ret_type
        try:
            # Username query is case insensitive
            query = {username_field + '__iexact': username}
            user = User.objects.get(**query)
            if user.check_password(password):
                return user, ret_type
        except User.DoesNotExist:
            return None, ret_type

        return None, ret_type

    def _authenticate_by_email(self, **credentials):
        # Even though allauth will pass along `email`, other apps may
        # not respect this setting. For example, when using
        # django-tastypie basic authentication, the login is always
        # passed as `username`.  So let's place nice with other apps
        # and use username as fallback

        ret_type = 'email'
        email = credentials.get('email', credentials.get('username'))

        if email:
            for user in filter_users_by_email(email):
                if user.check_password(credentials["password"]):
                    return user, ret_type
        return None, ret_type

    def _authenticate_by_phone(self, **credentials):
        phone_field = phone_quick_signup_app_settings.USER_MODEL_PHONE_FIELD
        phone = credentials.get('phone', credentials.get('username'))
        password = credentials.get('password')
        ret_type = 'phone'

        User = get_user_model()

        query = {phone_field + '__iexact': phone}
        users = User.objects.filter(**query)
        for user in users:
            if user.check_password(password):
                return user, ret_type

        return None, ret_type
