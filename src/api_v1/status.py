# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

USER_ACCOUNT_DISABLED = 100000
USER_ACCOUNT_DELETED = 100001
USERNAME_OR_PASSWORD_INCORRECT = 100002
USERNAME_AND_PASSWORD_REQUIRED = 100003
EMAIL_IS_NOT_VERIFIED = 100004
PHONE_IS_NOT_VERIFIED = 100005
EMAIL_AND_PHONE_IS_NOT_VERIFIED = 100006
EMAIL_OR_PHONE_IS_NOT_VERIFIED = 100007
PASSWORD_FIELD_REQUIRED = 100008
PASSWORD_CONFIRMATION_FIELD_REQUIRED = 100009
PASSWORD_MISMATCH = 100010
PASSWORD_INVALID = 100011
PASSWORD_CHANGE_PARAMS_INCORRECT = 100012
PASSWORD_CHANGE_SUCCESS = 100013
SOCIAL_LOGIN_PARAMS_REQUIRED = 100014
REQUEST_METHOD_NOT_ALLOWED = 100015
CHAT_RECORD_PARAMS_REQUIRED = 100016
DEVICE_INFO_UNIQUE_ID_EXIST = 100017

STATUS_DICT = {
    USER_ACCOUNT_DISABLED: _('Your account is disabled, please contact administrator'),
    USER_ACCOUNT_DELETED: _('Your account does not exist or is deleted'),
    USERNAME_OR_PASSWORD_INCORRECT: _('Username or password incorrect'),
    USERNAME_AND_PASSWORD_REQUIRED: _('Must include "username" and "password"'),
    EMAIL_IS_NOT_VERIFIED: _('E-mail is not verified'),
    PHONE_IS_NOT_VERIFIED: _('Phone is not verified'),
    EMAIL_AND_PHONE_IS_NOT_VERIFIED: _('E-mail and phone is not verified'),
    EMAIL_OR_PHONE_IS_NOT_VERIFIED: _('E-mail or phone is not verified'),
    PASSWORD_FIELD_REQUIRED: _('Must include "new_password1"'),
    PASSWORD_CONFIRMATION_FIELD_REQUIRED: _('Must include "new_password2"'),
    PASSWORD_MISMATCH: _('The two password fields didn\'t match'),
    PASSWORD_INVALID: _('Invalid password'),
    PASSWORD_CHANGE_PARAMS_INCORRECT: _('Password change parameters incorrect'),
    PASSWORD_CHANGE_SUCCESS: _('New password has been saved'),
    SOCIAL_LOGIN_PARAMS_REQUIRED: _('Social login parameters required'),
    REQUEST_METHOD_NOT_ALLOWED: _('Request method not allowed'),
    CHAT_RECORD_PARAMS_REQUIRED: _('Chat record parameters required'),
    DEVICE_INFO_UNIQUE_ID_EXIST: _('Device info unique id exist'),
}


class ApiStatus:
    status_code = None
    status_message = None

    def __init__(self, status_code, status_message=None):
        self.status_code = status_code
        self.status_message = status_message

    def get_message(self):
        for key, value in STATUS_DICT.items():
            if key == self.status_code:
                return value
        return None

    def get_json(self):
        if not self.status_message:
            self.status_message = self.get_message()

        return {
            'code': self.status_code,
            'message': self.status_message
        }
