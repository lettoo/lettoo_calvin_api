from django.conf.urls import url

from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    url(r'^qq/login/$', views.SocialQQView.as_view()),
    url(r'^weibo/login/$', views.SocialWeiboView.as_view()),
    url(r'^wechat/login/$', views.SocialWechatView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
