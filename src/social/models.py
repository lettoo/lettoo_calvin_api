# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from allauth.socialaccount.fields import JSONField

from src.users.models import User


class SocialQQ(TimeStampedModel):
    openid = models.CharField(_('openid'), max_length=255, unique=True, primary_key=True)
    access_token = models.CharField(_('access_token'), max_length=255)
    oauth_consumer_key = models.CharField(_('oauth_consumer_key'), max_length=255)
    user = models.ForeignKey(User, null=True, blank=True)
    extra_data = JSONField(null=True, blank=True, default='{}')

    def __unicode__(self):
        return self.access_token


class SocialWeibo(TimeStampedModel):
    uid = models.CharField(_('uid'), max_length=255, unique=True, primary_key=True)
    access_token = models.CharField(_('access_token'), max_length=255)
    user = models.ForeignKey(User, null=True, blank=True)
    extra_data = JSONField(null=True, blank=True, default='{}')

    def __unicode__(self):
        return self.access_token


class SocialWechat(TimeStampedModel):
    code = models.CharField(max_length=255)
    openid = models.CharField(max_length=255, unique=True)
    appid = models.CharField(max_length=255)
    access_token = models.CharField(max_length=255, blank=True, null=True)
    user = models.ForeignKey(User, null=True, blank=True)
    extra_data1 = JSONField(null=True, blank=True, default='{}')
    extra_data2 = JSONField(null=True, blank=True, default='{}')

    def __unicode__(self):
        return self.code
