# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import SocialQQ, SocialWeibo, SocialWechat


@admin.register(SocialQQ)
class SocialQQAdmin(admin.ModelAdmin):
    list_display = ['access_token', 'openid', 'oauth_consumer_key', 'user', 'extra_data', 'created']
    raw_id_fields = ['user']


@admin.register(SocialWeibo)
class SocialWeiboAdmin(admin.ModelAdmin):
    list_display = ['access_token', 'uid', 'user', 'extra_data', 'created']
    raw_id_fields = ['user']


@admin.register(SocialWechat)
class SocialWechatAdmin(admin.ModelAdmin):
    list_display = ['access_token', 'appid', 'user', 'extra_data1', 'extra_data2', 'created']
    raw_id_fields = ['user']