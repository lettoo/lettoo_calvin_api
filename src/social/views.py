# -*- coding: utf-8 -*-

import time
import datetime
from django.conf import settings
import requests
from allauth.utils import generate_unique_username

from django.contrib.auth import get_user_model

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from src.api_v1.status import ApiStatus, SOCIAL_LOGIN_PARAMS_REQUIRED, REQUEST_METHOD_NOT_ALLOWED
from src.social.models import SocialQQ, SocialWeibo, SocialWechat


class SocialQQView(APIView):
    """
    QQ social login

    Parameters
    ----------
    request

    Returns
    -------

    """

    permission_classes = (AllowAny,)
    allowed_methods = ('POST', 'OPTIONS', 'HEAD')
    token_model = Token
    user_model = get_user_model()

    def get(self, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def put(self, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def delete(self, request, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def patch(self, request, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def post(self, request, *args, **kwargs):
        try:
            json_data = request.data
            access_token = json_data.get('access_token', None)
            openid = json_data.get('openid', None)
            oauth_consumer_key = json_data.get('oauth_consumer_key', None)

            if access_token and openid and oauth_consumer_key:
                social_qq, created = SocialQQ.objects.get_or_create(openid=openid)

                if created:
                    social_qq.access_token = access_token
                    social_qq.oauth_consumer_key = oauth_consumer_key

                    # Get qq info
                    resp = requests.get(
                        url='https://graph.qq.com/user/get_user_info',
                        params={
                            'access_token': access_token,
                            'openid': openid,
                            'oauth_consumer_key': oauth_consumer_key,
                            'format': 'json'
                        },
                        headers={'Content-Type': 'application/json'}
                    )

                    social_qq.extra_data = resp.json()

                    # Create user model
                    name = social_qq.extra_data['nickname']
                    avatar_url = social_qq.extra_data['figureurl_2']
                    username = '%s%s%s' % (
                        openid[0:10], str('%.0f' % time.time()), str(datetime.datetime.now().microsecond))
                    user = self.user_model(username=username, name=name, avatar_url=avatar_url)
                    user.save()
                    social_qq.user = user

                    # Save social qq models
                    social_qq.save()

                # Get user token and response data
                token, created = self.token_model.objects.get_or_create(user=social_qq.user)
                result = social_qq.user.dict()
                result['key'] = token.key
                return Response(result, status=status.HTTP_200_OK)
            else:
                return Response(ApiStatus(SOCIAL_LOGIN_PARAMS_REQUIRED).get_json(), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'code': 500, 'message': e.message}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class SocialWeiboView(APIView):
    """
    QQ social login

    Parameters
    ----------
    request

    Returns
    -------

    """

    permission_classes = (AllowAny,)
    allowed_methods = ('POST', 'OPTIONS', 'HEAD')
    token_model = Token
    user_model = get_user_model()

    def get(self, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def put(self, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def delete(self, request, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def patch(self, request, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def post(self, request, *args, **kwargs):
        try:
            json_data = request.data
            access_token = json_data.get('access_token', None)
            uid = json_data.get('uid', None)

            if access_token and uid:
                social_weibo, created = SocialWeibo.objects.get_or_create(uid=uid)

                if created:
                    social_weibo.access_token = access_token

                    # Get weibo info
                    resp = requests.get(
                        url='https://api.weibo.com/2/users/show.json',
                        params={
                            'access_token': access_token,
                            'uid': uid,
                        },
                        headers={'Content-Type': 'application/json'}
                    )

                    social_weibo.extra_data = resp.json()

                    # Create user model
                    name = social_weibo.extra_data['screen_name']
                    avatar_url = social_weibo.extra_data['profile_image_url']
                    username = uid
                    user = self.user_model(username=username, name=name, avatar_url=avatar_url)
                    user.save()
                    social_weibo.user = user

                    # Save social weibo models
                    social_weibo.save()

                # Get user token and response data
                token, created = self.token_model.objects.get_or_create(user=social_weibo.user)
                result = social_weibo.user.dict()
                result['key'] = token.key
                return Response(result, status=status.HTTP_200_OK)
            else:
                return Response(ApiStatus(SOCIAL_LOGIN_PARAMS_REQUIRED).get_json(), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'code': 500, 'message': e.message}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class SocialWechatView(APIView):
    """
    Wechat social login

    Parameters
    ----------
    request

    Returns
    -------

    """

    permission_classes = (AllowAny,)
    allowed_methods = ('POST', 'OPTIONS', 'HEAD')
    token_model = Token
    user_model = get_user_model()

    def get(self, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def put(self, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def delete(self, request, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def patch(self, request, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def post(self, request, *args, **kwargs):
        try:
            json_data = request.data
            code = json_data.get('code', None)
            appid = json_data.get('appid', None)

            if code and appid:
                # Get access_token at first
                resp = requests.get(
                    url='https://api.weixin.qq.com/sns/oauth2/access_token',
                    params={
                        'appid': appid,
                        'secret': settings.WECHAT_SECRET_KEY,
                        'code': code,
                        'grant_type': 'authorization_code',
                    },
                    headers={'Content-Type': 'application/json'}
                )
                extra_data1 = resp.json()
                openid = extra_data1['openid']

                social_wechat, created = SocialWechat.objects.get_or_create(openid=openid, appid=appid)
                if created:
                    social_wechat.code = code
                    social_wechat.access_token = extra_data1['access_token']
                    social_wechat.extra_data1 = extra_data1

                    # Get the user info
                    resp = requests.get(
                        url='https://api.weixin.qq.com/sns/userinfo',
                        params={
                            'access_token': social_wechat.access_token,
                            'openid': social_wechat.extra_data1['openid'],
                        },
                        headers={'Content-Type': 'application/json'}
                    )
                    extra_data2 = resp.json()
                    social_wechat.extra_data2 = extra_data2

                    # Create user model
                    name = social_wechat.extra_data2['nickname']
                    avatar_url = social_wechat.extra_data2['headimgurl']
                    username = social_wechat.extra_data2['openid']
                    user = self.user_model.objects.get_or_create(username=username)[0]
                    user.name = name
                    user.avatar_url = avatar_url
                    user.save()
                    social_wechat.user = user

                    # Save social weibo models
                    social_wechat.save()

                # Get user token and response data
                token, created = self.token_model.objects.get_or_create(user=social_wechat.user)
                result = social_wechat.user.dict()
                result['key'] = token.key
                return Response(result, status=status.HTTP_200_OK)
            else:
                return Response(ApiStatus(SOCIAL_LOGIN_PARAMS_REQUIRED).get_json(), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'code': 500, 'message': e.message}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)