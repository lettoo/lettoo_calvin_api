# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils.models import TimeStampedModel
from src.users.models import User


class DeviceInfo(TimeStampedModel):
    device_unique_id = models.CharField(_('Device Unique ID'), primary_key=True, unique=True, max_length=255)
    device_manufacturer = models.CharField(_('Device Manufacturer'), null=True, blank=True, max_length=255)
    device_model = models.CharField(_('Device Model'), null=True, blank=True, max_length=255)
    device_id = models.CharField(_('Device ID'), null=True, blank=True, max_length=255)
    system_name = models.CharField(_('System Name'), null=True, blank=True, max_length=255)
    device_version = models.CharField(_('Device Version'), null=True, blank=True, max_length=255)
    bundle_id = models.CharField(_('Bundle Id'), null=True, blank=True, max_length=255)
    build_number = models.CharField(_('Build Number'), null=True, blank=True, max_length=255)
    app_version = models.CharField(_('App Version'), null=True, blank=True, max_length=255)
    app_version_readable = models.CharField(_('App Version (Readable)'), null=True, blank=True, max_length=255)
    device_name = models.CharField(_('Device Name'), null=True, blank=True, max_length=255)
    user_agent = models.CharField(_('User Agent'), null=True, blank=True, max_length=255)
    device_locale = models.CharField(_('Device Locale'), null=True, blank=True, max_length=255)
    device_country = models.CharField(_('Device Country'), null=True, blank=True, max_length=255)
    registration_id = models.CharField(_('JPush Registration ID'), null=True, blank=True, max_length=255)
    user = models.ForeignKey(User, null=True, blank=True)

    class Meta:
        ordering = ('-modified',)

    def __unicode__(self):
        return u'%s' % self.device_unique_id
