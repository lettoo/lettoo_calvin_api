# -*- coding: utf-8 -*-

from django.contrib.auth import get_user_model

from rest_framework import serializers

from src.device.models import DeviceInfo

USER_MODEL = get_user_model()


class DeviceInfoSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    class Meta:
        model = DeviceInfo

    def create(self, validated_data):
        user = validated_data['user']

        if not user.is_authenticated():
            validated_data.pop('user')

        device_info = super(DeviceInfoSerializer, self).create(validated_data)
        return device_info

    def get_user(self, obj):
        if obj.user:
            return {
                'id': obj.user.pk,
                'username': obj.user.username,
                'nickname': obj.user.name,
                'first_name': obj.user.first_name,
                'last_name': obj.user.last_name,
                'avatar_url': obj.user.avatar_url
            }
        else:
            return None


class DeviceInfoDetailSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    class Meta:
        model = DeviceInfo

    def update(self, instance, validated_data):
        user = validated_data['user']

        if not user.is_authenticated():
            validated_data.pop('user')

        device_info = super(DeviceInfoDetailSerializer, self).update(instance, validated_data)
        return device_info

    def get_user(self, obj):
        if obj.user:
            return {
                'id': obj.user.pk,
                'username': obj.user.username,
                'nickname': obj.user.name,
                'first_name': obj.user.first_name,
                'last_name': obj.user.last_name,
                'avatar_url': obj.user.avatar_url
            }
        else:
            return None
