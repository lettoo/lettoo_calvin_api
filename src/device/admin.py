# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import DeviceInfo


@admin.register(DeviceInfo)
class DeviceInfoAdmin(admin.ModelAdmin):
    list_display = ['user', 'device_unique_id', 'device_manufacturer', 'device_model', 'device_id', 'system_name',
                    'device_version', 'bundle_id', 'build_number', 'app_version', 'app_version_readable', 'device_name',
                    'user_agent', 'device_locale', 'device_country', 'registration_id', 'created']
    raw_id_fields = ['user']
