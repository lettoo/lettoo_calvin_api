# -*- coding: utf-8 -*-

from rest_framework import generics
from rest_framework import filters
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from rest_framework import permissions
from rest_framework.response import Response

from src.api_v1.status import ApiStatus, DEVICE_INFO_UNIQUE_ID_EXIST
from src.device.filters import DeviceInfoFilter
from src.device.models import DeviceInfo
from src.device.serializers import DeviceInfoSerializer, DeviceInfoDetailSerializer


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 1000


class DeviceInfoList(generics.ListCreateAPIView):
    queryset = DeviceInfo.objects.select_related().all()
    serializer_class = DeviceInfoSerializer
    permission_classes = (permissions.AllowAny,)
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_class = DeviceInfoFilter
    filter_fields = ('device_unique_id', 'registration_id')
    search_fields = ('device_unique_id', 'registration_id', 'system_name', 'device_name', 'user__name')

    def post(self, request, *args, **kwargs):
        device_unique_id = request.data['device_unique_id']

        if DeviceInfo.objects.filter(pk=device_unique_id).exists():
            return Response(ApiStatus(DEVICE_INFO_UNIQUE_ID_EXIST).get_json(),
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            result = self.create(request, *args, **kwargs)

        return result

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class DeviceInfoDetail(generics.RetrieveUpdateAPIView):
    queryset = DeviceInfo.objects.select_related().all()
    serializer_class = DeviceInfoDetailSerializer
    permission_classes = (permissions.AllowAny,)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)
