# -*- coding: utf-8 -*-

import django_filters
from django.db.models import Q

from src.device.models import DeviceInfo


class DeviceInfoFilter(django_filters.FilterSet):
    registration_id__isnull = django_filters.MethodFilter(action='no_registration_id_filter')
    registration_id__exclude = django_filters.MethodFilter(action='exclude_registration_id_filter')
    user__isnull = django_filters.MethodFilter(action='no_user_filter')

    class Meta:
        model = DeviceInfo

    def no_registration_id_filter(self, queryset, value):
        if value == 'True':
            return queryset.filter(Q(registration_id__isnull=True) | Q(registration_id=''))
        else:
            return queryset.filter(registration_id__isnull=False).exclude(registration_id='')

    def exclude_registration_id_filter(self, queryset, value):
        if value:
            exclude_list = value.split(',')
        else:
            exclude_list = []

        if exclude_list and len(exclude_list) > 0:
            return queryset.exclude(registration_id__in=exclude_list)
        else:
            return queryset

    def no_user_filter(self, queryset, value):
        if value == 'True':
            return queryset.filter(user__isnull=True)
        else:
            return queryset.filter(user__isnull=False)
