# -*- coding: utf-8 -*-

from django.contrib.auth import get_user_model

from rest_framework import generics
from rest_framework import filters
from rest_framework.pagination import PageNumberPagination
from rest_framework import permissions

from src.news.models import News, Comment, Like
from src.news.serializers import NewsSerializer, NewsDetailSerializer, CommentSerializer, LikeSerializer

USER_MODEL = get_user_model()


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 1000


class NewsList(generics.ListAPIView):
    queryset = News.objects.select_related().all()
    serializer_class = NewsSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('title', 'content', 'type')
    search_fields = ('title', 'content', 'type')


class NewsDetail(generics.RetrieveAPIView):
    queryset = News.objects.select_related().all()
    serializer_class = NewsDetailSerializer


class CommentList(generics.ListCreateAPIView):
    queryset = Comment.objects.select_related().all()
    serializer_class = CommentSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('content', 'news')
    search_fields = ('content',)

    def get_queryset(self):
        return Comment.objects.select_related().filter(approved=True)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class LikeList(generics.ListCreateAPIView):
    queryset = Like.objects.select_related().all()
    serializer_class = LikeSerializer
    permission_classes = (permissions.AllowAny,)
    pagination_class = StandardResultsSetPagination

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
