# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth import get_user_model

from rest_framework import serializers

from src.api_v1.serializers import AuthUserDetailSerializer
from src.news.models import News, Comment, Like

USER_MODEL = get_user_model()


class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""

    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        return value


class NewsSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()
    image_url = serializers.SerializerMethodField()
    like_count = serializers.SerializerMethodField()
    comment_count = serializers.SerializerMethodField()

    class Meta:
        model = News
        exclude = ('image', 'author')

    def get_image_url(self, obj):
        request = self.context.get('request')
        return obj.get_small_thumbnail(request)

    def get_like_count(self, obj):
        return Like.objects.filter(news=obj).count()

    def get_comment_count(self, obj):
        return Comment.objects.filter(news=obj, approved=True).count()

    def get_author(self, obj):
        return {
            'id': obj.author.pk,
            'username': obj.author.username,
            'nickname': obj.author.name,
            'first_name': obj.author.first_name,
            'last_name': obj.author.last_name,
            'avatar_url': obj.author.avatar_url
        }


class NewsDetailSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()
    image_url = serializers.SerializerMethodField()
    like_count = serializers.SerializerMethodField()
    comment_count = serializers.SerializerMethodField()

    class Meta:
        model = News
        exclude = ('image', 'author')

    def get_image_url(self, obj):
        request = self.context.get('request')
        return obj.get_small_thumbnail(request)

    def get_like_count(self, obj):
        return Like.objects.filter(news=obj).count()

    def get_comment_count(self, obj):
        return Comment.objects.filter(news=obj, approved=True).count()

    def get_author(self, obj):
        return {
            'id': obj.author.pk,
            'username': obj.author.username,
            'first_name': obj.author.first_name,
            'last_name': obj.author.last_name,
            'avatar_url': obj.author.avatar_url
        }


class CommentSerializer(serializers.ModelSerializer):
    content = serializers.CharField(required=True)
    user = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        exclude = ('approved',)

    def create(self, validated_data):
        if settings.AUTO_APPROVE_NEWS_COMMENT:
            validated_data['approved'] = True

        comment = super(CommentSerializer, self).create(validated_data)

        return comment

    def get_user(self, obj):
        return {
            'id': obj.user.pk,
            'nickname': obj.user.name,
            'username': obj.user.username,
            'first_name': obj.user.first_name,
            'last_name': obj.user.last_name,
            'avatar_url': obj.user.avatar_url
        }


class LikeSerializer(serializers.ModelSerializer):
    user = AuthUserDetailSerializer(read_only=True)

    class Meta:
        model = Like

    def create(self, validated_data):
        user = validated_data['user']

        if not user.is_authenticated():
            validated_data.pop('user')

        like = super(LikeSerializer, self).create(validated_data)

        return like
