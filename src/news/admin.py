# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import News, Comment, Like


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ['title', 'content', 'author', 'type', 'image', 'created']
    raw_id_fields = ['author']


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['user', 'news', 'content', 'approved', 'created']
    raw_id_fields = ['user', 'news']


@admin.register(Like)
class LikeAdmin(admin.ModelAdmin):
    list_display = ['user', 'news', 'created']
    raw_id_fields = ['user', 'news']
