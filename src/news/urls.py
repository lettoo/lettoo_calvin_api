from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views


urlpatterns = [
    url(r'^$', views.NewsList.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', views.NewsDetail.as_view()),
    url(r'^comment/$', views.CommentList.as_view()),
    url(r'^like/$', views.LikeList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
