# -*- coding: utf-8 -*-

import os
from uuid import uuid4

from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from allauth.utils import build_absolute_uri

from sorl.thumbnail import ImageField, get_thumbnail
from src.users.models import User


def get_news_image_filename(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (str(uuid4()), ext)
    file_path = 'news/%s/' % instance.pk
    return os.path.join(file_path, filename)


class News(TimeStampedModel):
    title = models.CharField(_('title'), max_length=255)
    summary = models.CharField(max_length=255, null=True, blank=True)
    content = models.TextField(_('content'))
    author = models.ForeignKey(User)
    type = models.CharField(_('type'), max_length=20)
    image = ImageField(upload_to=get_news_image_filename)

    def __unicode__(self):
        return u'%s' % self.title

    class Meta:
        ordering = ('-modified',)

    def get_absolute_url(self, request=None):
        url = self.image.url
        url = build_absolute_uri(request, url)
        return url

    def get_small_thumbnail(self, request=None):
        url = get_thumbnail(self.image, '856x535', crop='center', quality=99).url
        return build_absolute_uri(request, url)


class Comment(TimeStampedModel):
    user = models.ForeignKey(User)
    news = models.ForeignKey(News)
    content = models.TextField(_('content'), null=True, blank=True)
    approved = models.BooleanField(_('approved'), default=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.content


class Like(TimeStampedModel):
    user = models.ForeignKey(User, null=True, blank=True)
    news = models.ForeignKey(News)
