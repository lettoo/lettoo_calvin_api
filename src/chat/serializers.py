# -*- coding: utf-8 -*-

from django.contrib.auth import get_user_model

from rest_framework import serializers

from src.chat.models import ChatRecord

USER_MODEL = get_user_model()


class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""

    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        return value


class ChatRecordSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    class Meta:
        model = ChatRecord

    def get_user(self, obj):
        return {
            'id': obj.user.pk,
            'username': obj.user.username,
            'nickname': obj.user.name,
            'first_name': obj.user.first_name,
            'last_name': obj.user.last_name,
            'avatar_url': obj.user.avatar_url
        }
