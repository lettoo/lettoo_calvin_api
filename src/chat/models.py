# -*- coding: utf-8 -*-

from datetime import datetime
from time import mktime

from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel

from src.users.models import User


class ChatRecord(TimeStampedModel):
    user = models.ForeignKey(User)
    content = models.TextField(_('content'))
    timestamp = models.BigIntegerField(_('timestamp'))
    is_receive = models.BooleanField(default=False)

    class Meta:
        ordering = ('-timestamp',)

    def __unicode__(self):
        return u'%s' % self.content

    def set_timestamp(self):
        dt = datetime.utcnow()
        sec_since_epoch = mktime(dt.timetuple()) + dt.microsecond / 1000000.0
        millis_since_epoch = sec_since_epoch * 1000

        self.timestamp = int(millis_since_epoch)

    def save(self, *args, **kwargs):
        is_create = False if self.id else True

        if is_create:
            self.set_timestamp()

        super(self.__class__, self).save(*args, **kwargs)
