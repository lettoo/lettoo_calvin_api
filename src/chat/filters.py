import django_filters
from django.contrib.auth import get_user_model
from src.chat.models import ChatRecord

USER_MODEL = get_user_model()


class ChatRecordFilter(django_filters.FilterSet):
    class Meta:
        model = ChatRecord
        fields = {'timestamp': ['exact', 'gt', 'gte', 'lt', 'lte']}
