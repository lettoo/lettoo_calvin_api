# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import ChatRecord


@admin.register(ChatRecord)
class ChatRecordAdmin(admin.ModelAdmin):
    list_display = ['user', 'content', 'timestamp', 'created']
    raw_id_fields = ['user']
