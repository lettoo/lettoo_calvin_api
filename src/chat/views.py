# -*- coding: utf-8 -*-

import requests

from django.conf import settings
from django.contrib.auth import get_user_model

from rest_framework import generics
from rest_framework import filters
from rest_framework.pagination import PageNumberPagination
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from src.api_v1.status import REQUEST_METHOD_NOT_ALLOWED, ApiStatus, CHAT_RECORD_PARAMS_REQUIRED
from src.chat.filters import ChatRecordFilter
from src.chat.models import ChatRecord
from src.chat.serializers import ChatRecordSerializer

USER_MODEL = get_user_model()


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 1000


class ChatRecordList(generics.ListAPIView):
    queryset = ChatRecord.objects.select_related().all()
    serializer_class = ChatRecordSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = StandardResultsSetPagination
    filter_class = ChatRecordFilter
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('content', 'timestamp')
    search_fields = ('content',)

    def get_queryset(self):
        return ChatRecord.objects.select_related().filter(user=self.request.user)


class ChatRecordView(APIView):
    """
    Chat record post view

    Parameters
    ----------
    request

    Returns
    -------

    """

    permission_classes = (permissions.IsAuthenticated,)
    allowed_methods = ('POST', 'OPTIONS', 'HEAD')
    user_model = get_user_model()

    def get(self, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def put(self, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def delete(self, request, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def patch(self, request, *args, **kwargs):
        return Response(ApiStatus(REQUEST_METHOD_NOT_ALLOWED).get_json(), status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def post(self, request, *args, **kwargs):
        try:
            content = request.data.get('content', None)

            if content:
                send_chat_record = ChatRecord.objects.create(user=request.user, content=content)

                # Request turing robot api
                resp = requests.post(
                    url='http://www.tuling123.com/openapi/api',
                    params={
                        'key': settings.TURING_ROBOT_API_KEY,
                        'info': send_chat_record.content,
                        'userid': send_chat_record.user.pk
                    },
                    headers={'Content-Type': 'application/json'}
                )

                result = resp.json()
                if result['code'] == 100000:
                    receive_chat_record = ChatRecord.objects.create(user=request.user, content=result['text'],
                                                                    is_receive=True)
                    return Response({
                        'send': {
                            'id': send_chat_record.pk,
                            'content': send_chat_record.content,
                            'timestamp': send_chat_record.timestamp,
                            'is_receive': send_chat_record.is_receive,
                            'created': send_chat_record.created
                        },
                        'receive': {
                            'id': receive_chat_record.pk,
                            'content': receive_chat_record.content,
                            'timestamp': receive_chat_record.timestamp,
                            'is_receive': receive_chat_record.is_receive,
                            'created': receive_chat_record.created
                        }
                    }, status=status.HTTP_200_OK)
                else:
                    return Response({'code': 500, 'message': result['text']},
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                return Response(ApiStatus(CHAT_RECORD_PARAMS_REQUIRED).get_json(), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'code': 500, 'message': e.message}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
