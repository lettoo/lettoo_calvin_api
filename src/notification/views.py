# -*- coding: utf-8 -*-

from rest_framework import generics
from rest_framework import filters
from rest_framework.pagination import PageNumberPagination

from src.api_v1.permissions import IsSuperUser
from src.notification.models import Notification
from src.notification.serializers import NotificationSerializer, NotificationDetailSerializer


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 1000


class NotificationList(generics.ListCreateAPIView):
    queryset = Notification.objects.select_related().all()
    serializer_class = NotificationSerializer
    permission_classes = (IsSuperUser,)
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('title', 'alert')
    search_fields = ('title', 'alert')


class NotificationDetail(generics.RetrieveAPIView):
    queryset = Notification.objects.select_related().all()
    serializer_class = NotificationDetailSerializer
    permission_classes = (IsSuperUser,)
