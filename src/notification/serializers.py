# -*- coding: utf-8 -*-

from django.conf import settings
from django.contrib.auth import get_user_model

from rest_framework import serializers

from src.notification.models import Notification
from src.notification.tasks import send_jpush_notification_task

USER_MODEL = get_user_model()


class NotificationSerializer(serializers.ModelSerializer):
    receive = serializers.SerializerMethodField()

    class Meta:
        model = Notification

    def create(self, validated_data):
        notification = super(NotificationSerializer, self).create(validated_data)
        if settings.DEBUG:
            send_jpush_notification_task(notification)
        else:
            send_jpush_notification_task.delay(notification)
        return notification

    def get_receive(self, obj):
        try:
            if obj.receive:
                return {
                    'msg_id': obj.receive.msg_id,
                    'android_received': obj.receive.android_received,
                    'ios_apns_sent': obj.receive.ios_apns_sent
                }
            else:
                return None
        except Exception as e:
            return None


class NotificationDetailSerializer(serializers.ModelSerializer):
    receive = serializers.SerializerMethodField()

    class Meta:
        model = Notification

    def get_receive(self, obj):
        try:
            if obj.receive:
                return {
                    'msg_id': obj.receive.msg_id,
                    'android_received': obj.receive.android_received,
                    'ios_apns_sent': obj.receive.ios_apns_sent
                }
            else:
                return None
        except Exception as e:
            return None
