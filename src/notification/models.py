# -*- coding: utf-8 -*-

from django.db import models
from allauth.socialaccount.fields import JSONField

from model_utils.models import TimeStampedModel


class Notification(TimeStampedModel):
    title = models.CharField(null=True, blank=True, max_length=255)
    alert = models.CharField(max_length=255)
    extras = JSONField(default={})
    sound = models.CharField(default='default', max_length=255)
    badge = models.PositiveIntegerField(null=True, blank=True)
    audience = JSONField(default={})
    platform = JSONField(default=[])
    is_send = models.BooleanField(default=False)

    class Meta:
        ordering = ('-modified',)

    def __unicode__(self):
        return u'%s' % self.alert


class NotificationReceive(TimeStampedModel):
    notification = models.OneToOneField(Notification, related_name='receive')
    msg_id = models.CharField(max_length=255, unique=True)
    android_received = models.PositiveIntegerField(null=True, blank=True)
    ios_apns_sent = models.PositiveIntegerField(null=True, blank=True)
    is_receive = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s' % self.msg_id
