# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import Notification, NotificationReceive


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display = ['title', 'alert', 'extras', 'sound', 'badge', 'audience', 'platform', 'is_send', 'created']


@admin.register(NotificationReceive)
class NotificationReceiveAdmin(admin.ModelAdmin):
    list_display = ['notification', 'msg_id', 'android_received', 'ios_apns_sent', 'is_receive', 'created']
    raw_id_fields = ['notification']
