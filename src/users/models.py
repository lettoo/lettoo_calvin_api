# -*- coding: utf-8 -*-

import random
import os
from uuid import uuid4

from PIL import Image
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.files import File
from django.core import validators
from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from allauth.utils import build_absolute_uri
from six import python_2_unicode_compatible

from sorl.thumbnail import ImageField, get_thumbnail
from src.api_v1.utils import get_file_md5


@python_2_unicode_compatible
class User(AbstractUser):
    # First Name and Last Name do not cover name patterns
    # around the globe.
    phone = models.CharField(_('phone number'), blank=True, max_length=30)
    name = models.CharField(_("name of user"), blank=True, max_length=255)
    deleted = models.BooleanField(_('deleted'), default=False)
    phone_verified = models.BooleanField(_('phone verified'), default=False)
    email_verified = models.BooleanField(_('email verified'), default=False)
    avatar_url = models.URLField(_('avatar url'), null=True, blank=True)

    def __str__(self):
        return u'%s' % self.username

    def dict(self):
        return {
            'id': self.id,
            'username': self.username,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'name': self.name,
            'email': self.email,
            'phone': self.phone,
            'phone_verified': self.phone_verified,
            'email_verified': self.email_verified,
            'has_usable_password': self.has_usable_password(),
            'avatar_url': self.avatar_url
        }

    def save(self, *args, **kwargs):
        super(self.__class__, self).save(*args, **kwargs)

        if not self.avatar_url and settings.DEBUG is False:
            image_path = '%s/images/%s.jpg' % (settings.STATICFILES_DIRS[0], random.randint(1, 9))
            image = Image.open(image_path)
            image_file = File(image.fp)

            md5 = get_file_md5(image_file)
            avatar, exist = check_avatar_exist_with_md5(self, md5)
            if not exist:
                size = image_file.size
                width = image.width
                height = image.height
                avatar = UserAvatar(user=self, md5=md5, size=size, width=width, height=height)
                avatar.picture.save(image_file.name, image_file)
                avatar.save()

            self.avatar_url = avatar.get_small_thumbnail()
            self.save()



def get_avatar_filename(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (str(uuid4()), ext)
    file_path = 'avatar/%s/' % instance.user
    return os.path.join(file_path, filename)


def check_avatar_exist_with_md5(user, md5):
    try:
        p = UserAvatar.objects.filter(md5=md5, user=user)[0]
        return p, True
    except Exception:
        return None, False


class UserAvatar(TimeStampedModel):
    user = models.ForeignKey(User, editable=False)
    picture = ImageField(upload_to=get_avatar_filename)
    width = models.PositiveIntegerField(default=0)
    height = models.PositiveIntegerField(default=0)
    size = models.PositiveIntegerField(default=0)
    md5 = models.CharField(max_length=255, null=True, blank=True)

    def get_absolute_url(self, request=None):
        url = self.picture.url
        url = build_absolute_uri(request, url)
        return url

    def get_small_thumbnail(self, request=None):
        url = get_thumbnail(self.picture, '128x128', crop='center', quality=99).url
        return build_absolute_uri(request, url)
