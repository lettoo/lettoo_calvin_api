# -*- coding: utf-8 -*-

from django.db import models

from model_utils.models import TimeStampedModel


class Contact(TimeStampedModel):
    name = models.CharField(null=True, blank=True, max_length=255)
    phone = models.CharField(null=True, blank=True, max_length=255)
    email = models.EmailField(null=True, blank=True, max_length=255)
    subject = models.CharField(null=True, blank=True, max_length=255)
    content = models.CharField(max_length=255)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.content
