# -*- coding: utf-8 -*-

from django.contrib.auth import get_user_model

from rest_framework import serializers

from src.contact.models import Contact

USER_MODEL = get_user_model()


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
