# -*- coding: utf-8 -*-

from __future__ import absolute_import

import datetime
import json
import requests
import base64

from django.conf import settings

from celery.utils.log import get_task_logger
from celery import shared_task

from src.notification.models import NotificationReceive

logger = get_task_logger(__name__)


@shared_task
def send_jpush_notification_task(obj):
    try:
        authorization = 'Basic %s' % base64.b64encode('%s:%s' % (settings.JPUSH_APPKEY, settings.JPUSH_MASTER_SECRET))

        data = {
            'platform': obj.platform if obj.platform else 'all',
            'audience': obj.audience if obj.audience else 'all',
            'notification': {
                'alert': obj.alert,
                'android': {
                    'extras': obj.extras
                },
                'ios': {
                    'sound': obj.sound,
                    'extras': obj.extras
                }
            }
        }

        if obj.title:
            data['notification']['android']['title'] = obj.title

        if obj.badge:
            data['notification']['ios']['badge'] = obj.badge

        response = requests.post(
            url='https://api.jpush.cn/v3/push',
            data=json.dumps(data),
            headers={
                'content-type': 'application/json',
                'authorization': authorization,
                'cache-control': 'no-cache'
            })

        if response.status_code == 200:
            msg_id = response.json().get('msg_id', None)
            if msg_id:
                NotificationReceive.objects.create(notification=obj, msg_id=msg_id)
                obj.is_send = True
                obj.save()
    except Exception as e:
        logger.error(e)


@shared_task
def update_jpush_notificatioon_receive_task():
    try:
        receive_list = NotificationReceive.objects.filter(is_receive=False)

        authorization = 'Basic %s' % base64.b64encode('%s:%s' % (settings.JPUSH_APPKEY, settings.JPUSH_MASTER_SECRET))

        for item in receive_list:
            response = requests.get(
                url='https://report.jpush.cn/v3/received',
                params={
                    'msg_ids': item.msg_id
                },
                headers={
                    'content-type': 'application/json',
                    'authorization': authorization,
                    'cache-control': 'no-cache'
                })

            if response.status_code == 200:
                item.android_received = response.json()[0]['android_received']
                item.ios_apns_sent = response.json()[0]['ios_apns_sent']

                created = item.created.replace(tzinfo=None)
                delta = datetime.datetime.now() - created

                if delta > datetime.timedelta(seconds=50):
                    item.is_receive = True
                item.save()
    except Exception as e:
        logger.error(e)
