# -*- coding: utf-8 -*-

from rest_framework import generics
from rest_framework import filters
from rest_framework.pagination import PageNumberPagination
from rest_framework import permissions

from src.contact.models import Contact
from src.contact.serializers import ContactSerializer


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 1000


class ContactList(generics.ListCreateAPIView):
    queryset = Contact.objects.select_related().all()
    serializer_class = ContactSerializer
    permission_classes = (permissions.AllowAny,)
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('name', 'phone', 'email', 'subject', 'content')
    search_fields = ('name', 'phone', 'email', 'subject', 'content')
