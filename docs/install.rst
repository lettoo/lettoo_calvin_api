Install
=========

This is where you write how to get a new laptop to run this project.

CREATE USER lettoo_base_api WITH PASSWORD 'password';
CREATE DATABASE lettoo_base_api OWNER lettoo_base_api;
GRANT ALL PRIVILEGES ON DATABASE lettoo_base_api to lettoo_base_api;


$vi /var/lib/pgsql/data/pg_hba.conf
local	all	all	peer > local	all	all	md5
$ systemctl restart postgresql.service
$ psql -U lettoo_base_api -d lettoo_base_api
