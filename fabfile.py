"""
On local machine:
$ source env/bin/activate
$ fab api deploy
$ fab admin_console deploy
Root password: P@ssw0rd!@#
"""

from fabric.api import *


def api():
    env.hosts = ['182.92.180.163']
    env.user = 'ecs-user'
    env.key_filename = 'lettoo_base.pem'
    env.project_dir = '/home/ecs-user/lettoo_base_api'
    env.virtualenv = '/home/ecs-user/lettoo_base_api/env'
    env.branch = 'master'
    env.type = 'api'


def admin_console():
    env.hosts = ['182.92.180.163']
    env.user = 'ecs-user'
    env.key_filename = 'lettoo_base.pem'
    env.project_dir = '/home/ecs-user/lettoo_base_admin_console'
    env.branch = 'master'
    env.type = 'admin_console'


def pull():
    "Pull new code"
    with cd(env.project_dir):
        command = 'git checkout {branch}'.format(branch=env.branch)
        run(command)
        command = 'git pull origin {branch}'.format(branch=env.branch)
        run(command)


def update_requirements():
    "Update requirements in the virtualenv."
    with cd(env.project_dir):
        with prefix("source %s/bin/activate" % env.virtualenv):
            run("pip install -r requirements.txt")


def migrate(app=None):
    with cd(env.project_dir):
        with prefix("source %s/bin/activate" % env.virtualenv):
            if app:
                run('python manage.py migrate --settings=config.settings.production --noinput %s' % app)
            else:
                run('python manage.py migrate --settings=config.settings.production --noinput')


def collectstatic():
    with cd(env.project_dir):
        with prefix("source %s/bin/activate" % env.virtualenv):
            run('python manage.py collectstatic --settings=config.settings.production --noinput')


def compilemessages():
    with cd(env.project_dir):
        with prefix("source %s/bin/activate" % env.virtualenv):
            run('python manage.py compilemessages --settings=config.settings.production')


def restart_supervisor():
    "Restart (or just start) the server"
    with cd(env.project_dir):
        with prefix("source %s/bin/activate" % env.virtualenv):
            run('supervisorctl restart all')


def restart_nginx():
    sudo('systemctl restart nginx.service')


def npm():
    with cd(env.project_dir):
        run('npm install')


def bower():
    with cd(env.project_dir):
        run('bower install')


def gulp():
    with cd(env.project_dir):
        run('gulp')


def deploy():
    if env.type == 'api':
        pull()
        update_requirements()
        migrate()
        collectstatic()
        compilemessages()
        restart_supervisor()
        restart_nginx
    else:
        pull()
        npm()
        bower()
        gulp()
        restart_nginx()
