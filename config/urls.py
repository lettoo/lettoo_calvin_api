# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views

from src.api_v1.views import LoginView, PasswordChangeView

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='pages/home.html'), name="home"),

    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, include(admin.site.urls)),

    url(r'^api/v1/auth/login/$', LoginView.as_view(), name='rest_login'),
    url(r'^api/v1/auth/password/change/$', PasswordChangeView.as_view(), name='rest_password_change'),

    # Your stuff: custom urls includes go here
    url(r'^api/v1/auth/', include('rest_auth.urls')),
    url(r'^api/v1/signup/', include('rest_auth.registration.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^api/v1/', include('src.api_v1.urls')),
    url(r'^api/v1/email-quick/', include('email_quick_signup.urls')),
    url(r'^api/v1/phone-quick/', include('phone_quick_signup.urls')),
    url(r'^api/v1/news/', include('src.news.urls')),
    url(r'^api/v1/social/', include('src.social.urls')),
    url(r'^api/v1/chat/', include('src.chat.urls')),
    url(r'^api/v1/device/', include('src.device.urls')),
    url(r'^api/v1/notification/', include('src.notification.urls')),
    url(r'^api/v1/contact/', include('src.contact.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception("Bad Request!")}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception("Permission Denied")}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception("Page not Found")}),
        url(r'^500/$', default_views.server_error),
    ]
